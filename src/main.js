import Vue from 'vue';
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import VueSimpleSVG from 'vue-simple-svg';
import App from './App.vue';
import router from './router';
import store from './store';

Vue.config.productionTip = false;
Vue.use(ElementUI);
Vue.use(VueSimpleSVG);

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');
