import Vue from 'vue';
import Router from 'vue-router';
import Home from './components/Home.vue';
import Store from './store/index';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      redirect: {
        name: 'workspace',
        params: {
          workspace: Object.keys(Store.state.apps)[0],
        },
      },
    },
    {
      path: '/:workspace',
      name: 'workspace',
      component: Home,
      beforeEnter: (to, from, next) => {
        if (Object.keys(Store.state.apps).includes(to.params.workspace)) {
          Store.dispatch('setWorkspace', to.params.workspace);
          next();
        } else {
          next('/');
        }
      },
    },
    {
      path: '*',
      name: '404',
      redirect: { name: 'home' },
    },
  ],
});
