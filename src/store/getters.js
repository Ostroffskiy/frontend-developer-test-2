export default {
  filteredApps(state) {
    const workspace = state.currentWorkspace;
    const rate = state.filters[workspace] ? state.filters[workspace].rate || 0 : 0;
    const platform = state.filters[workspace] ? state.filters[workspace].platform || 'all' : 'all';
    const apps = state.apps[workspace].applications;
    if (!Object.keys(apps).length) return {};

    const filteredByRate = apps.filter((item) => {
      const itemRate = parseFloat(item.rating.replace(',', '.'));
      return itemRate > parseFloat(rate);
    });

    const filteredByPlatform = platform === 'all'
      ? Array.from(apps)
      : apps.filter((item) => {
        if (platform === 'apple') {
          return item.platform === 'apple' || item.platform === 'iphone';
        }
        return item.platform === platform;
      });

    return filteredByPlatform
      .filter(item => filteredByRate
        .find(inner => item.categoryPosition === inner.categoryPosition));
  },
};
