import Vue from 'vue';

export default {
  setCurrentWorkspace(state, name) {
    Vue.set(state, 'currentWorkspace', name);
  },
  setWorkspaceFilter(state, payload) {
    if (!state.filters[payload.name]) {
      Vue.set(state.filters, payload.name, {});
    }
    Vue.set(state.filters[payload.name], payload.key, payload.value);
  },
};
