import * as apps from './apps.json';

const filters = JSON.parse(window.localStorage.getItem('filters'));

export default {
  apps: apps.default,
  currentWorkspace: '',
  filters: filters || {},
};
