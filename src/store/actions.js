export default {
  setWorkspace({ commit }, name) {
    commit('setCurrentWorkspace', name);
  },
  setWorkspaceFilter({ commit, dispatch }, payload) {
    commit('setWorkspaceFilter', payload);
    dispatch('saveFiltersState');
  },
  saveFiltersState({ state }) {
    const tmp = Object.assign({}, state.filters);
    window.localStorage.setItem('filters', JSON.stringify(tmp));
  },
};
